﻿using Microsoft.EntityFrameworkCore;
using DataMaster.Models;

namespace DataMaster
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }
        public DbSet<Student> Students { set; get; }
        public DbSet<Subject> Subjects { set; get; }
        public DbSet<StudentSubject> StudentSubjects { set; get; }
    }
}
