﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataMaster.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DataMaster.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentSubjectController : BaseController
    {
        ApplicationDbContext db;

        public StudentSubjectController(ApplicationDbContext db)
        {
            this.db = db;
        }
        // GET: api/<UserSubjectController>
        [HttpGet]
        public object Get()
        {
            return db.StudentSubjects.ToList();
        }

        // POST api/<UserSubjectController>
        [HttpPost]
        public object Post([FromBody] StudentSubject studentSubject)
        {
            db.StudentSubjects.Add(studentSubject);
            return db.SaveChanges();
        }
    }
}
