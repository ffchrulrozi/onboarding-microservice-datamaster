﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using DataMaster.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DataMaster.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : BaseController
    {
        private ApplicationDbContext db;
        public StudentController(ApplicationDbContext db)
        {
            this.db = db;
        }
        // GET: api/<UserController>
        [HttpGet]
        public object Get()
        {
            return db.Students.ToList();
        }

        // POST api/<UserController>
        [HttpPost]
        public object Post([FromBody] Student student)
        {
            db.Students.Add(student);
            return db.SaveChanges();
        }
    }
}
