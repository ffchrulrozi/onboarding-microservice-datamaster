﻿using DataMaster.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DataMaster.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : BaseController
    {
        public ApplicationDbContext db;
        public SubjectController(ApplicationDbContext db)
        {
            this.db = db;
        }
        // GET: api/<SubjectController>
        [HttpGet]
        public object Get()
        {
            return db.Subjects.ToList();
        }

        // GET api/<SubjectController>/5
        [HttpGet("{id}")]
        public object Get(int id)
        {
            var data = from sub in db.Subjects where sub.Id == id select sub.Name;
            return data;
        }

        // POST api/<SubjectController>
        [HttpPost]
        public object Post([FromBody] Subject subject)
        {
            db.Subjects.Add(subject);
            return db.SaveChanges();
        }
    }
}
