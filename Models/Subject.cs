﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMaster.Models
{
    public class Subject
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
