﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataMaster.Models
{
    public class StudentSubject
    {
        public int Id { set; get; }
        public int StudentNim { set; get; }
        public int SubjectId { set; get; }
        public virtual Student Student { set; get; }
        public virtual Subject Subject { set; get; }
    }
}
