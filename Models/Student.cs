﻿using System.ComponentModel.DataAnnotations;

namespace DataMaster.Models
{
    public class Student
    {
        [Key]
        public int Nim { set; get; }
        public string Name { set; get; }
        public string MajorId { set; get; }
    }
}
