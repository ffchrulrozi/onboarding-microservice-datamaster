﻿using System.ComponentModel.DataAnnotations;

namespace DataMaster.Models
{
    public class Major
    {
        public int ID { set; get; }
        public string Name { set; get; }
    }
}
